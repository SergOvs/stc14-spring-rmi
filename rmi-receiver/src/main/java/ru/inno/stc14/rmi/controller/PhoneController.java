package ru.inno.stc14.rmi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.inno.stc14.rmi.api.PhoneService;

@Controller
public class PhoneController {

    private PhoneService phoneService;

    @Autowired
    public PhoneController(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @RequestMapping("/phone")
    public String getPhone(Model model) {
        model.addAttribute("phone", phoneService.getPhone());
        model.addAttribute("object", phoneService.getSomeObject());
        return "phone";
    }
}
