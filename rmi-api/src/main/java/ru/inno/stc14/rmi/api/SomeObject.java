package ru.inno.stc14.rmi.api;

import java.io.Serializable;

public class SomeObject implements Serializable {

    private static final long serialVersionUID = -2875314006147111104L;

    private String name;
    private int age;

    public SomeObject(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "SomeObject{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
