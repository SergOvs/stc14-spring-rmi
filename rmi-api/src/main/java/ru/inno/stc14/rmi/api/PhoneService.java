package ru.inno.stc14.rmi.api;

public interface PhoneService {

    String getPhone();

    SomeObject getSomeObject();

}
