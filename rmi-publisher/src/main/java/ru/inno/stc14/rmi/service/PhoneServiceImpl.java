package ru.inno.stc14.rmi.service;

import ru.inno.stc14.rmi.api.PhoneService;
import ru.inno.stc14.rmi.api.SomeObject;

public class PhoneServiceImpl implements PhoneService {

    public String getPhone() {
        return "2-12-85-06";
    }

    @Override
    public SomeObject getSomeObject() {
        return new SomeObject("John Smyth", 23);
    }

}
